import React, { useEffect, useState } from "react";
import HomePage from "./homePage";
import Search from "./content/Search";
const Main = (props) => {
  const [klik, setKlik] = useState(false);
  const onKlik = (value) => {
    setKlik(value);
  };
  useEffect(() => {
    if (props.klik) setKlik(true);
  }, [props.klik]);

  return <main>{klik ? <Search /> : <HomePage onKlik={onKlik} />}</main>;
};
export default Main;
