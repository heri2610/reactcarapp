import OurService from "./OurServices";
import WhyUs from "./WhyUs";
import Testimoni from "./Testimoni";
import Banner from "./Banner";
import FAQ from "./FAQ";

const HomePage = ({ onKlik }) => {
  const onKlik2 = (value) => {
    onKlik(value);
  };
  return (
    <section className="HomePage">
      <OurService />
      <WhyUs />
      <Testimoni />
      <Banner con="container" onKlik={onKlik2} />
      <FAQ />
    </section>
  );
};
export default HomePage;
