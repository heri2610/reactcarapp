import clas from "./css/Banner.module.css";
const Banner = (props) => {
  const handleKlik = () => {
    props.onKlik(true);
  };
  return (
    <div className={`${props.con} ${clas.main}`}>
      <div className="row">
        <div className={clas.mainBanner}>
          <div className="col-lg-12 btn-primary text-center p-5">
            <h2>Sewa Mobil di (Lokasimu) Sekarang</h2>
            <p>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod <br />
              tempor incididunt ut labore et dolore magna aliqua.
            </p>
            <div className={clas.mainBannerButton}>
              <button id="tombol2" type="button" className="button-global" onClick={handleKlik}>
                Mulai Sewa Sekarang
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
export default Banner;
