import React from "react";
import left from "./img/left-button.png";
import right from "./img/right-button.png";
import johnDo from "./img/img-photo-1.png";
import rate from "./img/icon-rate.png";
import foto2 from "./img/img-photo-2.png";
import clas from "./css/Testimoni.module.css";
import OwlCarousel from "react-owl-carousel";
const Testimonial = () => {
  return (
    <div className={clas.mainTestimoni} id="Testimonial">
      <div className="container mt-5">
        <div className="row">
          <div className="col-12">
            <h4>Testimonial</h4>
            <p>Berbagai review positif dari para pelanggan kami</p>
          </div>
        </div>
      </div>
      <section className="container-fluid">
        <div className={clas.mainTestimoniCarousel}>
          <OwlCarousel
            className="owl-theme"
            loop
            nav
            center
            dots={false}
            margin={false}
            navText={[`<img src=${left} width='32px' height='32px'>`, `<img src=${right} width='32px' height='32px'>`]}
            responsive={{
              0: {
                items: 1,
              },
              600: {
                items: 2,
              },
              1000: {
                items: 2,
              },
            }}
          >
            <div className="item">
              <div className={clas.mainTestimoniDeks}>
                <div className="row">
                  <div className="col-2 offset-3">
                    <img className="img-fluid" src={rate} alt="icon rate" />
                  </div>
                </div>
                <div className="row">
                  <div className="col-2">
                    <div className={clas.mainTestimoniImages}>
                      <img src={johnDo} alt="uwong-Men" />
                    </div>
                  </div>
                  <div className="col-9 offset-1">
                    <p>
                      “Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod”
                    </p>
                  </div>
                </div>
                <div className="row">
                  <div className={clas.mainTestimoniName}>
                    <div className="col- offset-3">
                      <p>John Dee 32, Bromo</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="item">
              <div className={clas.mainTestimoniDeks}>
                <div className="row">
                  <div className="col-2 offset-3">
                    <img className="img-fluid" src={rate} alt="icon rate" />
                  </div>
                </div>
                <div className="row">
                  <div className="col-2">
                    <div className={clas.mainTestimoniImages}>
                      <img src={foto2} alt="uwong Grils" />
                    </div>
                  </div>
                  <div className="col-9 offset-1">
                    <p>
                      “Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod”
                    </p>
                  </div>
                </div>
                <div className="row">
                  <div className={clas.mainTestimoniName}>
                    <div className="col- offset-3">
                      <p>John Dee 32, Bromo</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="item">
              <div className={clas.mainTestimoniDeks}>
                <div className="row">
                  <div className="col-2 offset-3">
                    <img className="img-fluid" src={rate} alt="icon rate" />
                  </div>
                </div>
                <div className="row">
                  <div className="col-2">
                    <div className={clas.mainTestimoniImages}>
                      <img src={foto2} alt="uwong Gril" />
                    </div>
                  </div>
                  <div className="col-9 offset-1">
                    <p>
                      “Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod”
                    </p>
                  </div>
                </div>
                <div className="row">
                  <div className={clas.mainTestimoniName}>
                    <div className="col- offset-3">
                      <p>John Dee 32, Bromo</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </OwlCarousel>
        </div>
      </section>
    </div>
  );
};
export default Testimonial;
