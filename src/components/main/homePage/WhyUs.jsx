import imgComplate from "./img/icon-complete.png";
import imgJam from "./img/icon-24hrs.png";
import imgPrice from "./img/price-icon.png";
import imgProvesional from "./img/icon-professional.png";
const WhyUs = () => {
  return (
    <div className="container my-5" id="whyus">
      <div className="row main-whyus">
        <div className="col-12">
          <div className="main-whyus-title text-center">
            <h4>Why Us?</h4>
            <p>Mengapa harus pilih Binar Car Rental?</p>
          </div>
        </div>
        <div className="col-12 main-whyus-cards">
          <div className="row justify-content-center">
            <div className="col-md-6 col-lg-3 main-whyus-items">
              <div className="card">
                <div className="card-body">
                  <div className="row">
                    <div className="col-1">
                      <img src={imgComplate} width="32px" height="32px" alt="Complete" />
                    </div>
                    <div className="col-12">
                      <h5 className="card-title">Mobil Lengkap</h5>
                      <p className="card-text">Tersedia banyak pilihan mobil, kondisi masih baru, bersih dan terawat</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-md-6 col-lg-3 main-whyus-items">
              <div className="card">
                <div className="card-body">
                  <div className="row">
                    <div className="col-1">
                      <img src={imgPrice} width="32px" height="32px" alt="icon price" />
                    </div>
                    <div className="col-12">
                      <h5 className="card-title">Harga Murah</h5>
                      <p className="card-text">Harga murah dan bersaing, bisa bandingkan harga kami dengan rental mobil lain</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-md-6 col-lg-3 main-whyus-items">
              <div className="card">
                <div className="card-body">
                  <div className="row">
                    <div className="col-1">
                      <img src={imgJam} width="32px" height="32px" alt="icon 24hrs" />
                    </div>
                    <div className="col-12">
                      <h5 className="card-title">Layanan 24 Jam</h5>
                      <p className="card-text">Siap melayani kebutuhan Anda selama 24 jam nonstop. Kami juga tersedia di akhir minggu</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-md-6 col-lg-3 main-whyus-items">
              <div className="card">
                <div className="card-body">
                  <div className="row">
                    <div className="col-1">
                      <img src={imgProvesional} width="32px" height="32px" alt="icon professional" />
                    </div>
                    <div className="col-12">
                      <h5 className="card-title">Sopir Profesional</h5>
                      <p className="card-text">Sopir yang profesional, berpengalaman, jujur, ramah dan selalu tepat waktu</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
export default WhyUs;
