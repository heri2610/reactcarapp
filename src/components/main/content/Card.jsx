import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getCar } from "../../../actions/carAction";
const Card = () => {
  const { getCarResolve, getCarLoading, getCarError } = useSelector((state) => state.carReducer);
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getCar());
  }, [dispatch]);

  if (isLoading) return <h1>Loading data...</h1>;
  else if (data && !isError)
    return (
      <div class="card" style="width: 18rem;">
        <img src="..." class="card-img-top" alt="..." />
        <div class="card-body">
          <h5 class="card-title">Card title</h5>
          <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
          <a href="#" class="btn btn-primary">
            Go somewhere
          </a>
        </div>
      </div>
    );
  else {
    return <h1>Something Went Wrong</h1>;
  }
};

export default Card;
