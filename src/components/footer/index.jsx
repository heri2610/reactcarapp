import fb from "./img/facebook-icon.png";
import ig from "./img/instagram-icon.png";
import mail from "./img/mail-icon.png";
import twitch from "./img/twitch-icon.png";
import twitter from "./img/twitter-icon.png";
import clas from "./css/style.module.css";
const Footer = () => {
  return (
    <footer>
      <div className="container mt-5">
        <div className="row">
          <div className="col-lg-2 col-md-3 p-4">
            <p>Jalan Suroyo No. 161 Mayangan Kota Probolonggo 672000</p>
            <p>binarcarrental@gmail.com</p>
            <p>081-233-334-808</p>
          </div>
          <div className="ol-lg-2 col-md-3 p-4">
            <div className={clas.footerNav}>
              <a href="#Services">
                <p>Our services</p>
              </a>
              <a href="#whyus">
                <p>Why Us</p>
              </a>
              <a href="#Testimonial">
                <p>Testimonial</p>
              </a>
              <a href="#faq">
                <p>FAQ</p>
              </a>
            </div>
          </div>
          <div className="ol-lg-2 col-md-3 p-4">
            <p>Connect with us</p>
            <div className="footer-connect-icons">
              <div className="row">
                <div className="col-2">
                  <a href="https://www.facebook.com/suhaeri26">
                    <img href="instagram.com/gdariawhan" className="img-fluid" src={fb} alt="facebook" />
                  </a>
                </div>
                <div className="col-2">
                  <a href="https://www.instagram.com/heri_26">
                    <img className="img-fluid" src={ig} alt="instagram" />
                  </a>
                </div>
                <div className="col-2">
                  <a href="https://twitter.com/gdariawhan">
                    <img className="img-fluid" src={twitter} alt="twitter" />
                  </a>
                </div>
                <div className="col-2">
                  <a href="https://gmail.com">
                    <img className="img-fluid" src={mail} alt="mail" />
                  </a>
                </div>
                <div className="col-2">
                  <a href="https://www.twitch.tv/gdariawhan">
                    <img className="img-fluid" src={twitch} alt="twitch" />
                  </a>
                </div>
              </div>
            </div>
          </div>
          <div className="ol-lg-2 col-md-3 p-4">
            <p>Copyright Binar 2022</p>
            <a className={clas.footerLogo} href="index.html">
              {""}
            </a>
          </div>
        </div>
      </div>
    </footer>
  );
};
export default Footer;
