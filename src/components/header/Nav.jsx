import React from "react";
import { NavItem, NavbarBrand } from "reactstrap";
import clas from "./css/Nav.module.css";

function NavbarNav(props) {
  return (
    //
    <nav className="navbar navbar-expand-lg bgLight ">
      <div className="container">
        <NavbarBrand className={clas.headerNavbarLogo} href="/">
          {""}
        </NavbarBrand>
        <div className="collapse navbar-collapse justify-content-end bgLight" id="navbarNav">
          <ul className="navbar-nav mr-auto">
            <NavItem>
              <a className="nav-link" href="index.html#Services">
                Our Services
              </a>
            </NavItem>
            <NavItem>
              <a className="nav-link" href="index.html#whyus">
                Why Us
              </a>
            </NavItem>
            <NavItem>
              <a className="nav-link" href="index.html#Testimonial">
                Testimonial
              </a>
            </NavItem>
            <NavItem>
              <a className="nav-link" href="index.html#faq">
                FAQ
              </a>
            </NavItem>
            <NavItem>
              <button type="button" className="button-global" data-bs-toggle="modal" data-bs-target="#modalregister">
                Register
              </button>
            </NavItem>
          </ul>
        </div>
      </div>
    </nav>
  );
}

export default NavbarNav;
