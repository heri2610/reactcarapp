import Nav from "./Nav";
import car from "./img/img-car.svg";
import clas from "./css/style.module.css";
import { useState } from "react";
const Header = (props) => {
  const [klik, setKlik] = useState(true);
  const onKlik = () => {
    props.onKlik2(true);
    setKlik(false);
  };
  return (
    <header className={clas.header}>
      <Nav />
      <div className="container-fluid mt-1">
        <div className={clas.headerBanner}>
          <div className="row align-items-center intro">
            <div className="col-md-6 intro-text">
              <h1>
                Sewa & Rental Mobil Terbaik di <br />
                kawasan (Lokasimu)
              </h1>
              <p>
                Selamat datang di Binar Car Rental. Kami menyediakan mobil kualitas <br />
                terbaik dengan harga terjangkau. Selalu siap melayani kebutuhanmu <br />
                untuk sewa mobil selama 24 jam.
              </p>
              {klik && (
                <button type="button" className="button-global" id="tombol" onClick={onKlik}>
                  Mulai Sewa Mobil
                </button>
              )}
            </div>
            <div className="col-md-6 intro-img">
              <img className="img-fluid" src={car} alt={car} />
            </div>
          </div>
        </div>
      </div>
    </header>
  );
};
export default Header;
