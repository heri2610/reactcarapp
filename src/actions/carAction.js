import axios from "axios";

const GET_CAR = "GET_CAR";

export const grtCar = () => {
  return (dispatch) => {
    // loadng
    dispatch({
      type: GET_CAR,
      payload: {
        loading: true,
        data: false,
        errorMessage: false,
      },
    });
    // get API
    axios({
      methode: "GET",
      url: "https://raw.githubusercontent.com/fnurhidayat/probable-garbanz",
    })
      .then((response) => {
        // jika berhasil
        dispatch({
          type: GET_CAR,
          payload: {
            loading: false,
            data: response.data,
            errorMessage: false,
          },
        });
      })
      .catch((error) => {
        // jika gagal
        dispatch({
          type: GET_CAR,
          payload: {
            loading: false,
            data: false,
            errorMessage: error.message,
          },
        });
      });
  };
};
