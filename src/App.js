import React, { useState } from "react";
import "owl.carousel/dist/assets/owl.carousel.css";
import "owl.carousel/dist/assets/owl.theme.default.css";
import "./App.css";
import Header from "./components/header";
import Main from "./components/main";
import Footer from "./components/footer";

function App() {
  const [klik, setKlik] = useState(false);
  const onKlik2 = (value) => {
    setKlik(value);
  };
  return (
    <>
      <Header onKlik2={onKlik2} />
      <Main klik={klik} />
      <Footer />
    </>
  );
}

export default App;
