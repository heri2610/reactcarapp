import { GET_CAR } from "../../actions/carAction";
const initialState = {
  getCarResolve: false,
  getCarLoading: false,
  getCarError: false,
};

const car = (state = initialState, action) => {
  switch (action.type) {
    case GET_CAR:
      return {
        ...state,
        getCarResolve: action.payload.data,
        getCarLoading: action.payload.loading,
        getCarError: action.payload.errorMessage,
      };
    default:
      return state;
  }
};

export default car;
