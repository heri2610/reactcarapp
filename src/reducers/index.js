import { combineReducers } from "redux";
import carReducer from "./Cars";

export default combineReducers({ carReducer });
